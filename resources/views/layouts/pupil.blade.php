<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>@yield('title')</title>
        <meta name="generator" content="Bootply" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="{{url('sidebar/css/bootstrap.min.css')}}" rel="stylesheet">
        <!--[if lt IE 9]>
            <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="{{url('sidebar/js/jquery.js')}}"></script>
        <script src="{{url('sidebar/js/pupil.js')}}"></script>
        <link href="{{url('css/theme.css')}}" rel="stylesheet">
        <link href="{{url('sidebar/css/style.css')}}" rel="stylesheet">
    </head>
    <body>
            <div id="wrapper">
        <div class="overlay"></div>
    
        <!-- Sidebar -->
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">
                <li class="sidebar-brand">
                    <a href="{{url('/')}}">
                        მთავარი
                    </a>
                </li>
                <li>
                    <a href="{{url('/pupil')}}">პროფილი</a>
                </li>
                <li>
                    <a href="{{url('/pupil/mygrades')}}">ჩემი ნიშნები</a>
                </li>
                <li>
                    <a href="{{url('/pupil/attendance')}}">დასწრება</a>
                </li>
                <li>
                    <a href="{{url('/pupil/myhomeworks')}}">დავალებები</a>
                </li>
                <li>
                    <a href="{{url('/pupil/myschedule')}}">ჩემი ცხრილი</a>
                </li>
            </ul>
        </nav>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                <span class="hamb-top"></span>
                <span class="hamb-middle"></span>
                <span class="hamb-bottom"></span>
            </button>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                      @yield('content')                        
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->
        <script src="{{url('sidebar/js/scripts.js')}}"></script>
        <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        url='{{url('')}}';
    </script>
    </body>
</html>
