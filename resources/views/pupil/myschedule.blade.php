@extends('layouts.pupil')

@section('title', 'ცხრილი')

@section('content')
	<table  class="table table-striped table-bordered">
		<thead>
	      <tr>
	        <th>{{config('school.days.1')}}</th>
	        <th>{{config('school.days.2')}}</th>
	        <th>{{config('school.days.3')}}</th>
	        <th>{{config('school.days.4')}}</th>
	        <th>{{config('school.days.5')}}</th>
	        <th>{{config('school.days.6')}}</th>
	      </tr>
	    </thead>
		  <tr>
		  	@foreach($weekday as $day)

		  		<td>
	  				@foreach($day as $item)
		  				{{$item->order.')'.$item->subject->name.' '.$item->teacher->name.' '.$item->teacher->surname}}
		  				<br>
		  			@endforeach
		  		</td>
		  	@endforeach
		  </tr>
</table>



@endsection