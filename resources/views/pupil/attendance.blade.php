@extends('layouts.pupil')

@section('title', 'დასწრება')

@section('content')
	@foreach($subjects as $id=> $subject)
	<h4> {{$subject}}</h4>
		@if(count($attendance[$id])>0)
		<div class="well">
			@foreach($attendance[$id] as $att)
				
				     {{$att->date}}
				     @if(!$att->attendance)
				     <strong style="color:red" >{{$att->getAtt()}}</strong>
				     @else
				     <strong>{{$att->getAtt()}}</strong>
				     @endif
				     <br>
				
			@endforeach
		</div>
		<h5>დასწრება:{{$stat[$id]['daswreba']}}</h5>
		<h5>გაცდენა:{{$stat[$id]['gacdena']}}</h5>
		<h5>{{$stat[$id]['procenti']}}%</h5>
		@endif
	@endforeach
@endsection