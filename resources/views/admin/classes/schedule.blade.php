@extends('layouts.admin')

@section('title', 'კლასები')

@section('content')

<table  class="table table-striped">
		<thead>
	      <tr>
	        <th>{{config('school.days.1')}}</th>
	        <th>{{config('school.days.2')}}</th>
	        <th>{{config('school.days.3')}}</th>
	        <th>{{config('school.days.4')}}</th>
	        <th>{{config('school.days.5')}}</th>
	        <th>{{config('school.days.6')}}</th>
	      </tr>
	    </thead>
		  <tr>
		  	@foreach($weekday as $day)

		  		<td>
	  				@foreach($day as $item)
		  				{{$item->order.')'.$item->subject->name}}
		  				<br>
		  			@endforeach
		  		</td>
		  	@endforeach
		  </tr>
</table>

<div id="scheduleWrapper">

<div class="col-md-12 clearfix" id="schedulePicker">
	<div class="wrapper">
		<div class="col-md-3">
			{!!
				Form::select(
					'subject[]',
					$subjects,
					old('subject'),
					['class' => 'form-control subject']
				)
			!!}
		</div>
		<div class="col-md-3">
			{!!
				Form::select(
					'week_day[]',
					config('school.days'),
					old('week_day'),
					['class' => 'form-control day']
				)
			!!}
		</div>
		<div class="col-md-3">
			{!!
				Form::select(
					'teacher[]',
					$teacherSub[$first],
					old('teacher'),
					['class' => 'form-control teacher']
				)
			!!}
		</div>
		<div class="col-md-1">
			<input type="text" name="order[]" class="form-control" style="width: 40px" placeholder="N" >
		</div>

		<div class="col-md-2">
			<button type="button" id="addButton" class="btn btn-default">
				<i class="glyphicon glyphicon-plus"></i>
			</button>
		</div>
	</div>

</div>
<hr>
<hr>
<hr>
{!! Form::open([

		'url' => '/admin/classes/schedule/save/'.$classId,
		'method'=>'POST',
		'class' => 'well clearfix'

		]) !!}

	<div id="ScheduleForm" class="col-md-12">
		@foreach ($schedule as $item)
			<div class="wrapper">
				<div class="col-md-3">
					{!!
						Form::select(
							'subject[]',
							$subjects,
							$item->subject_id,
							['class' => 'form-control subject']
						)
					!!}
				</div>
				<div class="col-md-3">
					{!!
						Form::select(
							'week_day[]',
							config('school.days'),
							$item->week_day,
							['class' => 'form-control day']
						)
					!!}
				</div>
				<div class="col-md-3">
					{!!
						Form::select(
							'teacher[]',
							$teacherSub[$item->subject_id],
							$item->teacher_id,
							['class' => 'form-control teacher']
						)
					!!}
				</div>
				<div class="col-md-1">
					<input type="text" name="order[]" class="form-control" style="width: 40px" value="{{$item->order}}" placeholder="N" >
				</div>

				<div class="col-md-2">
					<button type="button" class="btn btn-default">
						<i class="glyphicon glyphicon-remove"></i>
					</button>
				</div>
			</div>
		@endforeach
	</div>
	<hr>

	<button type="submit" class="btn btn-success pull-right">Save</button>

{!! Form::close() !!}



</div>

@endsection
