@extends('layouts.admin')

@section('title', 'დამატება')

@section('content')
@if (count($errors) > 0)
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
			<p>{{ $error }}</p>
		@endforeach
	</div>
@endif

		{!! Form::open([
		
		'url' => '/admin/teachers',
		'method'=>'POST',
		'class'=>'form-horizontal well'

		]) !!}
	<div>
			<fieldset>

			<!-- Form Name -->
			<legend>მასწავლებლის დამატება</legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">User name</label>  
			  <div class="col-md-4">
			  <input  name="u_name" type="text" placeholder="იუზერის სახელი" class="form-control input-md">  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">Email</label>  
			  <div class="col-md-4">
			  <input  name="email" type="email" placeholder="მეილი" class="form-control input-md">  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">Password</label>  
			  <div class="col-md-4">
			  <input  name="password" type="password" class="form-control input-md">  
			  </div>
			</div>
			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">სახელი</label>  
			  <div class="col-md-4">
			  <input  name="name" type="text" placeholder="სახელი" class="form-control input-md">  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">გვარი</label>  
			  <div class="col-md-4">
			  <input  name="surname" type="text" placeholder="გვარი" class="form-control input-md">  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">პირადი ნომერი</label>  
			  <div class="col-md-4">
			  <input  name="personal_number" type="text" placeholder="პირადი ნომერი" class="form-control input-md">  
			  </div>
			</div>


			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" >საგნები</label>
			  <div class="col-md-4">
			  
				{!!
					Form::select(
						'subject[]',
						$subjects,
						old('subject'),
						['class'=>'form-control','multiple' => 'multiple']
						
					)
				!!}
				
			  </div>
			</div>
			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" >სადამრიგებლო კლასი</label>
			  <div class="col-md-4">
			 {!!
					Form::select(
						'class_id',
						$classes,
						old('class_id'),
						['class'=>'form-control']
						
						
					)
				!!}
			  </div>
			</div>
			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="singlebutton"></label>
			  <div class="col-md-4">
			    <button id="singlebutton" name="singlebutton" class="btn btn-primary">დამატება</button>
			  </div>
			</div>
			

			</fieldset>
		</div>
	{!! Form::close() !!}



@endsection