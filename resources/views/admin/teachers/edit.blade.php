@extends('layouts.admin')

@section('title', 'დამატება')

@section('content')

		{!! Form::open([
		
		'url' => '/admin/teachers/'.$teacher->id,
		'method'=>'PUT',
		'class'=>'form-horizontal well'

		]) !!}
	<div>
			<fieldset>

			<!-- Form Name -->
			<legend>მასწავლებლის რედაქტირება</legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">სახელი</label>  
			  <div class="col-md-4">
			  <input  name="name" value="{{old('name',$teacher->name)}}" type="text" placeholder="სახელი" class="form-control input-md">  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">გვარი</label>  
			  <div class="col-md-4">
			  <input  name="surname" value="{{old('surname',$teacher->surname)}}" type="text" placeholder="გვარი" class="form-control input-md">  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">პირადი ნომერი</label>  
			  <div class="col-md-4">
			  <input  name="personal_number" value="{{old('personal_number',$teacher->personal_number)}}" type="text" placeholder="პირადი ნომერი" class="form-control input-md">  
			  </div>
			</div>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" >საგნები</label>
			  <div class="col-md-4">
			  
				{!!
					Form::select(
						'subject[]',
						$subjects,
						old('subject',$selected_subs),
						['class'=>'form-control','multiple' => 'multiple']
						
					)
				!!}

				
			  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-4 control-label" >სადამრიგებლო კლასი</label>
			  <div class="col-md-4">
			 {!!
					Form::select(
						'class_id',
						$classes,
						old('class_id',$teacher->class_id),
						['class'=>'form-control']
						
						
					)
				!!}
			  </div>
			</div>
			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="singlebutton"></label>
			  <div class="col-md-4">
			    <button id="singlebutton" name="singlebutton" class="btn btn-primary">განახლება</button>
			  </div>
			</div>
			

			</fieldset>
		</div>
	{!! Form::close() !!}



@endsection