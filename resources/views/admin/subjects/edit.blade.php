@extends('layouts.admin')

@section('title', 'რედაქტირება')

@section('content')
@if (count($errors) > 0)
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
			<p>{{ $error }}</p>
		@endforeach
	</div>
@endif

	{!! Form::open([
		
		'url' => '/admin/subjects/'.$subject->id,
		'method'=>'PUT',
		'class'=>'form-horizontal well'

		]) !!}
	<div>
			<fieldset>

			<!-- Form Name -->
			<legend>საგნის რედაქტირება</legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">სახელი</label>  
			  <div class="col-md-4">
			  <input  name="name" type="text" value="{{old('title',$subject->name)}}" placeholder="სახელი" class="form-control input-md">  
			  </div>
			</div>

			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="singlebutton"></label>
			  <div class="col-md-4">
			    <button id="singlebutton" name="singlebutton" class="btn btn-primary">შეცვლა</button>
			  </div>
			</div>
			

			</fieldset>
		</div>
	{!! Form::close() !!}



@endsection