@extends('layouts.admin')

@section('title', 'მართვა')

@section('content')
	<a class="btn btn-success pull-right" href="{{url('/admin/pupils/add')}}" >Add</a>
	<table  class="table table-striped table-bordered">
		<thead>
	      <tr>
	        <th>Firstname</th>
	        <th>Lastname</th>
	        <th>Personal number</th>
	        <th>Year</th>
	        <th>Edit</th>
	        <th>Delete</th>
	      </tr>
	    </thead>
		@foreach($pupils as $pupil)
		  <tr>
		    <td>{{$pupil->name}}</td>
		    <td>{{$pupil->surname}}</td>
		    <td>{{$pupil->personal_number}}</td>
		    <td>{{$year-$pupil->enter_year}}</td>
		    <td><a href="{{url('/admin/pupils/edit/'.$pupil->id)}}">რედაქტირება</a></td>
		    <td><a href="{{url('/admin/pupils/delete/'.$pupil->id)}}" style="color:red">წაშლა</a></td>
		  </tr>
		@endforeach
	</table>
	{!! $pupils->render() !!}

@endsection