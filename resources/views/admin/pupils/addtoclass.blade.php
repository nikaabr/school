@extends('layouts.admin')

@section('title', 'კლასები')

@section('content')

	<div class="container">
	    <h2>კლასები</h2>
	    <form role="form" method="POST" action="{{url('/admin/pupils/addtoclass')}}">
		    <div class="form-group">
		      <label for="sel1">აირჩიე კლასი:</label>
		      <select class="form-control" name="id">
		      	@foreach($classes as $class)
		        	<option value="{{$class->id}}" >
		        		{{$year-$class->start_year .'' .$class->name}}
		        	</option>
		        @endforeach	
		      </select>
		  	</div>
		  	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		  	<button type="submit" class="btn btn-default">Submit</button>
		</form>  	
	</div>
@endsection