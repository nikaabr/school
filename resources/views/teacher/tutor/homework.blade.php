@extends('layouts.teacher')

@section('title', 'დავალებები')

@section('content')
	@foreach($subjects as $id=> $subject)
	<h4> {{$subject}}</h4>
		@if(count($homeworks[$id])>0)
		<div class="alert alert-info">
			@foreach($homeworks[$id] as $homework)
				
				     {{$homework->date}}
				     <strong>{{$homework->homework}}</strong>
				     <br>
				
			@endforeach
		</div>
		@endif
	@endforeach
@endsection