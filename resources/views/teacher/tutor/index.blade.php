@extends('layouts.teacher')

@section('title', 'სადამრიგებლო')

@section('content')
<h3>კლასი:{{$class->realName()}}</h3>
<hr>
<a class="pull-right" href="{{url('teacher/myclass/homeworks')}}">დავალებები</a>
<table  class="table table-striped">
		<thead>
	      <tr>
	        <th>მოსწავლე</th>
	        <th>პირადი ნომერი</th>
	        <th>დასწრება</th>
	      </tr>
	    </thead>
		@foreach($pupils as $pupil)
		  <tr>
		    <td>
		    	<a href="{{url('teacher/myclass/pupils/'.$pupil->id)}}">{{$pupil->name.' '.$pupil->surname}}</a>
		    </td>
		    <td>{{$pupil->personal_number}}</td>
		    <td><a href="{{url('teacher/myclass/pupils/'.$pupil->id.'/attendance')}}">ნახვა</a></td>
		  </tr>
		@endforeach
	</table>


@endsection