<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('attendance', [0, 1]);
            $table->integer('school_year_id')->unsigned();
            $table->integer('pupil_id')->unsigned();
            $table->integer('subject_id')->unsigned();
            $table->integer('teacher_id')->unsigned();
            $table->date('date');
            $table->foreign('pupil_id')->references('id')->on('pupils');
            $table->foreign('school_year_id')->references('id')->on('school_years');
            $table->foreign('subject_id')->references('id')->on('subjects');
            $table->foreign('teacher_id')->references('id')->on('teachers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attendances');
    }
}
