<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/



$factory->define(App\Models\Pupil::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName ,
        'personal_number' => $faker->unique()->ean8,
        'enter_year' =>$faker->numberBetween($min = 2005, $max = 2015),
    ];
});

$factory->define(App\Models\Teacher::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName ,
        'personal_number' => $faker->unique()->ean8,
        'class_id'=>$faker->unique()->numberBetween($min = 1, $max = 24),
    ];
});


