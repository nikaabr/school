<?php

use Illuminate\Database\Seeder;

class SchedulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schedules')->delete();
        $insertArray = [
            [
                'class_id' => 1,
                'school_year_id' => 2,
                'subject_id' => 1,
                'teacher_id'=>1,
                'order' => 1,
                'week_day' => 1,
                'teacher_id'=>1
            ],
            [
                'class_id' => 1,
                'school_year_id' => 2,
                'subject_id' => 2,
                'teacher_id'=>2,
                'order' => 2,
                'week_day' => 1,
                'teacher_id'=>2
            ],
            [
                'class_id' => 1,
                'school_year_id' => 2,
                'subject_id' => 3,
                'teacher_id'=>3,
                'order' => 1,
                'week_day' => 3,
                'teacher_id'=>2
            ],
            [
                'class_id' => 1,
                'school_year_id' => 2,
                'subject_id' => 4,
                'teacher_id'=>4,
                'order' => 1,
                'week_day' => 4,
                'teacher_id'=>3
            ],
        ];
        DB::table('schedules')->insert($insertArray);
    }
}
