<?php

use Illuminate\Database\Seeder;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classes')->delete();
        $insertArray = [];
            for ($i=1; $i <13 ; $i++) { 
                $insertArray[]=[
                    'name' => 'ა',
                    'max_pupil' => 30,
                    'start_year' => 2003+$i,
                    
                ];
                $insertArray[]=[
                    'name' => 'ბ',
                    'max_pupil' => 30,
                    'start_year' => 2003+$i,
                    
                ];

            }
        DB::table('classes')->insert($insertArray);
    }
}
