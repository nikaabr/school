<?php

use Illuminate\Database\Seeder;

class SchoolYearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('school_years')->delete();
        $insertArray = [
        	[
            'school_year' => '2015-2016',
            'semester_type' =>'1',
            'active' => '0',
            ],
            [
            'school_year' => '2015-2016',
            'semester_type' =>'2',
            'active' => '1',
            ], 
            [
            'school_year' => '2016-2017',
            'semester_type' =>'1',
            'active' => '0',
            ], 
        ];
        DB::table('school_years')->insert($insertArray);
    }
}
