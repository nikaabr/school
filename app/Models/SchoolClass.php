<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
	protected $table = 'classes';

    public function subjects()
    {
        return $this->belongsToMany('App\Models\Subject','class_subject','class_start_year','subject_id');
    }

    public function pupils()
    {
        return $this->hasMany('App\Models\Pupil','class_id');
    }

    public function teacher()
    {
        return $this->hasOne('App\Models\Teacher','class_id');
    }

    public function realName()
    {
        $year = SchoolYear::active()->year();
        $name=$year-$this->start_year.$this->name;
        return $name;
    }
   

    
}
