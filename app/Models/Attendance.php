<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
	public  function getAtt()
	{
		if($this->attendance==0){
			
			return 'გაცდენა';
		}
		return 'დასწრება';
	}
}
