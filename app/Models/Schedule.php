<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{

    public function subject()
    {
        return $this->belongsTo('App\Models\Subject');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }

    public function SchoolClass()
    {
        return $this->belongsTo('App\Models\SchoolClass','class_id');
    }

    public static function weekSchedule($class_id, $schoolyear_id)
    {
        $weekday = [];
        $schedule = Schedule::where([
            ['school_year_id', $schoolyear_id],
            ['class_id', $class_id],
        ])->orderBy('order', 'asc')->get();

        for ($i = 1; $i <= count(config('school.days')); $i++) {
            $weekday[$i] = $schedule->where('week_day', (string) $i);
        }
        return $weekday;
    }

    public static function storeUpdate($count, $classId, $schoolyearId, $subject, $week_day, $order)
    {
        for ($i = 0; $i < $count; $i++) {
            $item = Schedule::where([
                ['class_id', $classId],
                ['school_year_id', $schoolyearId],
                ['week_day', config('school.days.' . $week_day[$i])],
                ['order', $order[$i]],
            ])->first();
            if (isset($item)) {
                $item->subject_id = $subject[$i];
                $item->order = $order[$i];
                $item->week_day = $week_day[$i];
                $item->class_id = $classId;
                $item->school_year_id = $schoolyearId;
                $item->save();
            } else {
                $schedule = new Schedule;
                $schedule->subject_id = $subject[$i];
                $schedule->order = $order[$i];
                $schedule->week_day = $week_day[$i];
                $schedule->class_id = $classId;
                $schedule->school_year_id = $schoolyearId;
                $schedule->save();
            }
        }
    }

    public static function getTeacherSubjects($schoolyear_id,$class_id,$teacher_id)
    {
        $schedule = Schedule::where([
            ['school_year_id', $schoolyear_id],
            ['class_id', $class_id],
            ['teacher_id', $teacher_id],
        ])->get();

        $subject=[];
        foreach ($schedule as  $item) {
            $subject[]=$item->subject;
        }
        return $subject;
    }

    public static function getClassSubjects($schoolyear_id,$class_id)
    {
        $schedule = Schedule::where([
            ['school_year_id', $schoolyear_id],
            ['class_id', $class_id],
        ])->get();

        $subject=[];
        foreach ($schedule as  $item) {
            $subject[]=$item->subject;
        }
        return $subject;
    }

}
