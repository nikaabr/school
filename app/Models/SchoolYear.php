<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolYear extends Model
{
	protected $table = 'school_years';

	public function year()
	{
		return explode('-', $this->school_year)[1];
	}

	 public function semester()
    {
    	if ($this->semester_type==1) {
    		return 'შემოდგომა';
    	}
    	elseif ($this->semester_type==2) {
    		return 'გაზაფუხლი';
    	}
    		return 0;
    }

    public function type()
    {
    	if ($this->active==1) {
    		return 'მიმდინარე';
    	}
    		return 'პასიური';
    }

    public function makeactive()
    {
       $activeyear=SchoolYear::where('active',1)->first();
       $activeyear->active=0;
       $activeyear->save();      
       $this->active=1;
    }

    public static function active()
    {
        return SchoolYear::where('active', 1)->first();
    }

    
}
