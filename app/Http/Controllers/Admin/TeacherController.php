<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Teacher;
use App\Models\Subject;
use App\Models\SchoolClass;
use App\Models\SchoolYear;

class TeacherController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
    	$teachers=Teacher::paginate(30);
    	return view('admin/teachers/index', ['teachers' => $teachers]);
    }

    public function add()
    {
        $subjects=Subject::all();
        foreach ($subjects as $sub) {
            $subs[$sub->id] = $sub->name;
        }
        $classes=SchoolClass::orderBy('start_year', 'desc')->get();
        $schoolyear=SchoolYear::active();
        $year=$schoolyear->year();
        //$classArr=[];
        $classArr[0]='none';
        foreach ($classes as $class) {
            $classArr[$class->id] = $year-$class->start_year.$class->name;
        }
        return view('admin/teachers/add',['subjects'=>$subs,'classes' => $classArr]);
    }


    public function store(Request $request)
    {   
        $this->validate($request, [
            'u_name' => 'required',
            'email' => 'required',
            'password' => 'required|min:6',
            'name' => 'required',
            'surname' => 'required',
            'personal_number' => 'required',
            'subject' => 'required',

        ]);
        $teacher_user=new User;
        $teacher_user->name=$request->u_name;
        $teacher_user->email=$request->email;
        $teacher_user->password=bcrypt($request->password);
        $teacher_user->save();
        if($request->class_id==0){
            $teacher_user->attachRole(2);
        }
        else{
            $teacher_user->roles()->sync([2,3]);
            $totur=Teacher::where('class_id',$request->class_id)->first();
            if(isset($totur)){
              $tutorUser=User::find($totur->user_id);
              if(isset($tutorUser)){
              $tutorUser->roles()->sync([2]);
              }
            }
        }
        

        $teacher=new Teacher;
        $teacher->name=$request->name;
        $teacher->surname=$request->surname;
        $teacher->personal_number=$request->personal_number;
        $teacher->user_id=$teacher_user->id;
        $teacher->makeClassTeacher($request->class_id);
        $teacher->save();
        $teacher->subjects()->attach($request->subject);
        
        return redirect('admin/teachers');
    }

    public function edit($id)
    {
        $teacher=Teacher::find($id);
        $subjects=Subject::all();
        $selected_subs = [];
        foreach ($teacher->subjects as $sub) {
            $selected_subs[] = $sub->id;
        }
        foreach ($subjects as $sub) {
            $subs[$sub->id] = $sub->name;
        }
        $classes=SchoolClass::orderBy('start_year', 'desc')->get();
        $schoolyear=SchoolYear::active();
        $year=$schoolyear->year();
        $classArr=[];
        $classArr[0]='none';
        foreach ($classes as $class) {
            $classArr[$class->id] = $year-$class->start_year.$class->name;
        }
        return view('admin/teachers/edit',
                ['teacher'=>$teacher,'subjects'=>$subs,'selected_subs'=>$selected_subs,'classes' => $classArr]);
    }

    public function update(Request $request,$id)
    {
        $teacher=Teacher::find($id);
        $teacher->name=$request->name;
        $teacher->surname=$request->surname;
        $teacher->personal_number=$request->personal_number;
        $teacher->makeClassTeacher($request->class_id);
        $teacher->save();
        $teacher->subjects()->detach();
        $teacher->subjects()->attach($request->subject);
        
        return redirect('admin/teachers');
    }

    
}