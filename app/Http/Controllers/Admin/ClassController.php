<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\Schedule;
use App\Models\SchoolClass;
use App\Models\SchoolYear;
use App\Models\Subject;
use App\Models\Teacher;
use Illuminate\Http\Request;

class ClassController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        $classes = SchoolClass::orderBy('start_year', 'desc')->get();
        $year = SchoolYear::active()->year();
        return view('admin/classes/index', ['classes' => $classes, 'year' => $year]);
    }

    public function add()
    {
        return view('admin/classes/add');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'start_year' => 'required',
            'name' => 'required|max:1',
            'max_pupil' => 'required',

        ]);
        $schoolclass = new SchoolClass;
        $schoolclass->name = $request->name;
        $year = SchoolYear::active()->year();
        $schoolclass->start_year = $year - $request->start_year;
        $schoolclass->max_pupil = $request->max_pupil;
        $schoolclass->save();
        return redirect('admin/classes');

    }

    public function edit($id)
    {

        $class = SchoolClass::find($id);
        $classArr = [];
        for ($i = 1; $i <= 12; $i++) {
            $classArr[$i] = $i;
        }
        $year = SchoolYear::active()->year();
        return view('admin/classes/edit', ['class' => $class, 'classes' => $classArr, 'year' => $year]);
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'start_year' => 'required',
            'name' => 'required|max:1',
            'max_pupil' => 'required',

        ]);
        $schoolclass = SchoolClass::find($id);
        $schoolclass->name = $request->name;
        $year = SchoolYear::active()->year();
        $schoolclass->start_year = $year - $request->start_year;
        $schoolclass->max_pupil = $request->max_pupil;
        $schoolclass->save();
        return redirect('admin/classes');
    }

    public function schedule($id)
    {
        $schoolyear = SchoolYear::active();
        $weekday = Schedule::weekSchedule($id, $schoolyear->id);
        $params = [
            'class_id' => $id,
            'school_year_id' => $schoolyear->id,
        ];
        $schedule = Schedule::where($params)->get();

        $subjects = Subject::all();
        foreach ($subjects as $sub) {
            $subs[$sub->id] = $sub->name;
            $teachers_sub[$sub->id]=Teacher::hasSubject($sub->id);
            foreach ($teachers_sub[$sub->id] as $value) {
                $teacherSub[$sub->id][$value->id] = $value->name . ' ' . $value->surname;
            }
            if(empty($teachers_sub[$sub->id])){
                //dump($sub->id);
                $teacherSub[$sub->id]=[];
            }

        }
        //dd($teacherSub);
        // $teachers = Teacher::all();
        // foreach ($teachers as $teacher) {
        //     $teachs[$teacher->id] = $teacher->name . ' ' . $teacher->surname;
        // }
       
        reset($subs);
        $first_key = key($subs);

        $viewData = [
            'weekday' => $weekday,
            'subjects' => $subs,
            //'teachers' => $teachs,
            'classId' => $id,
            'schedule' => $schedule,
            'teacherSub'=>$teacherSub,
            'first'=>$first_key

        ];

        return view('admin/classes/schedule', $viewData);
    }

    public function saveSchedule($classId, Request $request)
    {

        $count = count($request->subject);
        $schoolyear = SchoolYear::active();

        Schedule::where([
            'class_id' => $classId,
            'school_year_id' => $schoolyear->id,
        ])->delete();

        foreach ($request['subject'] as $k => $subject) {
            $schedule = new Schedule;
            $schedule->subject_id = $subject;
            $schedule->order = $request['order'][$k];
            $schedule->teacher_id = $request['teacher'][$k];
            $schedule->week_day = $request['week_day'][$k];
            $schedule->class_id = $classId;
            $schedule->school_year_id = $schoolyear->id;
            $schedule->save();
        }

        return redirect('admin/classes/schedule/' . $classId);
    }

    public function selectSubject($subjectId)
    {
        $teachers = Teacher::hasSubject($subjectId);
        return $teachers;

    }

    public function ajaxTest()
    {
        $arr = ['id' => 1, 'name' => 'test 1'];
        return $arr;
    }
}
