<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subject;
use Auth;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        $subjects = Subject::all();
        return view('admin/subjects/index', ['subjects' => $subjects]);
    }

    public function add()
    {
    
        return view('admin/subjects/add');
    }

    public function store(Request $request)
    {
         $this->validate($request, [
            'name' => 'required|min:3',

        ]);
        $subject = new Subject;
        $subject->name = $request->name;
        $subject->save();

        return redirect('admin/subjects');
    }

    public function edit($id)
    {
        $subject = Subject::find($id);
        return view('admin/subjects/edit', ['subject' => $subject]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:3',

        ]);
        $subject = Subject::find($id);
        $subject->name = $request->name;
        $subject->save();

        return redirect('admin/subjects');
    }

}
