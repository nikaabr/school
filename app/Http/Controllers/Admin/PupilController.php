<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\SchoolClass;
use App\Models\SchoolYear;
use App\Models\Pupil;

class PupilController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
    	$pupils=Pupil::orderBy('surname', 'asc')->paginate(30);
    	$schoolyear=SchoolYear::active();
        $year=explode('-', $schoolyear->school_year);
    	return view('admin/pupils/index',['pupils'=>$pupils,'year'=>$year[1]]);
    }

    // public function showClass()
    // {
    // 	$classes=SchoolClass::orderBy('start_year', 'desc')->get();
    //     $schoolyear=SchoolYear::where('active',1)->first();
    // 	return view('admin/pupils/addtoclass', ['classes' => $classes,'year'=>$schoolyear->year()]);
    // }

    // public function addToClass(Request $request)
    // {	
    // 	$classes=SchoolClass::all();
    // 	$class=SchoolClass::find($request->id);
    // 	$year=$class->start_year;
    // 	$pupils=Pupil::where('enter_year',$year)->paginate(30);;
    // 	$schoolyear=SchoolYear::where('active',1)->first();
    // 	$year=explode('-', $schoolyear->school_year);
    // 	return view('admin/pupils/show',['pupils'=>$pupils,'year'=>$year[1]]);

    // }

    public function add()
    {
        $classes=SchoolClass::orderBy('start_year', 'desc')->get();
        $schoolyear=SchoolYear::active();  
        return view('admin/pupils/add',['classes' => $classes,'year'=>$schoolyear->year()]);
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'u_name' => 'required',
            'email' => 'required',
            'password' => 'required|min:6',
            'name' => 'required',
            'surname' => 'required',
            'personal_number' => 'required',
            'class_id' => 'required',

        ]);
        $pupil_user=new User;
        $pupil_user->name=$request->u_name;
        $pupil_user->email=$request->email;
        $pupil_user->password=bcrypt($request->password);
        $pupil_user->save();
        $pupil_user->attachRole(4);


        $pupil=new Pupil;
        $pupil->name=$request->name;
        $pupil->surname=$request->surname;
        $pupil->personal_number=$request->personal_number;
        $pupil->enter_year=SchoolClass::find($request->class_id)->start_year;
        $pupil->class_id=$request->class_id;
        $pupil->user_id=$pupil_user->id;

        $pupil->save();
        return redirect('admin/pupils');
    }

    public function edit($id)
    {
        $pupil=Pupil::find($id);
        $classes=SchoolClass::orderBy('start_year', 'desc')->get();
        $schoolyear=SchoolYear::active();
        $year=$schoolyear->year();
        $classArr=[];
        foreach ($classes as $class) {
            $classArr[$class->id] = $year-$class->start_year.$class->name;
        }
        return view('admin/pupils/edit',['pupil'=>$pupil,'classes' => $classArr,'year'=>$schoolyear->year()]);
    }
    public function update(Request $request,$id)
    {
        $pupil=Pupil::find($id);
        $pupil->name=$request->name;
        $pupil->surname=$request->surname;
        $pupil->personal_number=$request->personal_number;
        $pupil->enter_year=SchoolClass::find($request->class_id)->start_year;
        $pupil->class_id=$request->class_id;
        $pupil->save();
        return redirect('admin/pupils');
    }






}