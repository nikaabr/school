<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;

use App\Models\Schedule;
use App\Models\SchoolClass;
use App\Models\SchoolYear;
use App\Models\Subject;
use App\Models\Pupil;
use App\Models\Teacher;
use App\Models\Grade;
use App\Models\Homework;
use App\Models\Attendance;
use Illuminate\Http\Request;
use Exception;

class TutorController extends Controller
{
	public function index()
	{	
		$teacher=Teacher::authTeacher();
		$class=SchoolClass::find($teacher->class_id);
		return view('teacher/tutor/index',['class'=>$class,'pupils'=>$class->pupils]);
	}

	public function pupilGrade($pupilId)
	{
		$teacher=Teacher::authTeacher();
		if(!$teacher->isPupilTutor($pupilId)){
			 throw new Exception("Error Processing Request", 1);

		}

		$schoolyear=SchoolYear::active();
		$subjects=Subject::all();
		$subs=[];

 		foreach ($subjects as $value) {
 			$subs[$value->id]=$value->name;
 		}

 		$grades=Grade::where([
 			['school_year_id',$schoolyear->id],
 			['pupil_id',$pupilId],
 		])->get();

		$class_subs=Schedule::where([
		 ['school_year_id',$schoolyear->id],
		 ['class_id',$teacher->class_id],
		])->get();
		$gradeSubject=[];
 		foreach ($class_subs as  $subject) {
 			$gradeSubject[$subject->subject_id]=[];
 		}

 		foreach ($grades as  $grade) {
 			$gradeSubject[$grade->subject_id][]=$grade;
 		}

 		$grade_options=[];

 		for ($i=1; $i <11 ; $i++) { 
 			$grade_options[$i]=$i;
 		}
 		$viewData=[
 			'subjects'=>$gradeSubject,
 			'subs'=>$subs,
 			'grade_options'=>$grade_options,
 			
 		];
		return view('teacher/tutor/pupilgrade',$viewData);
	}

	public function homework()
	{
		$schoolyear=SchoolYear::active();
		$teacher=Teacher::authTeacher();
		$subjects=Schedule::getClassSubjects($schoolyear->id,$teacher->class_id);
		$subs=[];
		foreach ($subjects as  $value) {
			$subs[$value->id]=$value->name;
		}

		$homeworks=Homework::orderBy('id', 'desc')->where([
			['school_year_id',$schoolyear->id],
 			['class_id',$teacher->class_id],
 		])->get();
 		$sub_homeworks=[];
 		foreach ($subs as $key => $value) {
 			$sub_homeworks[$key]=$homeworks->where('subject_id',$key);
 		}
 		$viewData=[
	 		'subjects'=>$subs,
	 		'homeworks'=>$sub_homeworks

 		];

 		return view('teacher/tutor/homework',$viewData);
	}

	public function attendance($pupilID)
	{	
		$teacher=Teacher::authTeacher();
		if(!$teacher->isPupilTutor($pupilID)){
			throw new Exception("Error Processing Request", 1);

		}
		$schoolyear=SchoolYear::active();
		
		$subjects=Schedule::getClassSubjects($schoolyear->id,$teacher->class_id);
		$subs=[];
		foreach ($subjects as  $value) {
			$subs[$value->id]=$value->name;
		}

		$attendances=Attendance::orderBy('id', 'desc')->where([
			['school_year_id',$schoolyear->id],
 			['pupil_id',$pupilID],
 		])->get();
 		$sub_att=[];
 		$att=[];
 		$sul=null;
 		foreach ($subs as $key => $value) {
 			$sub_att[$key]=$attendances->where('subject_id',$key);
 			$att[$key]['procenti']='---';
 			$att[$key]['daswreba']=$sub_att[$key]->where('attendance',"1")->count();
 			$att[$key]['gacdena']=$sub_att[$key]->where('attendance',"0")->count();
 			$sul=$att[$key]['daswreba']+$att[$key]['gacdena'];
 			if($sul>0){
 				$att[$key]['procenti']=
 				 round(($att[$key]['daswreba']/$sul)*100);
 			}

 		}
 		$viewData=[
	 		'subjects'=>$subs,
	 		'attendance'=>$sub_att,
	 		'stat'=>$att

 		];
 		return view('teacher/tutor/attendance',$viewData);
	}
}
