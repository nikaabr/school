<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;

use App\Models\Schedule;
use App\Models\SchoolClass;
use App\Models\SchoolYear;
use App\Models\Subject;
use App\Models\Pupil;
use App\Models\Teacher;
use App\Models\Grade;
use App\Models\Homework;
use Illuminate\Http\Request;
use Exception;

class HomeworkController extends Controller
{
	public function index($classID)
	{
		
		$schoolyear=SchoolYear::active();
		$teacher=Teacher::authTeacher();
		if(!$teacher->hasclass($classID,$schoolyear->id)){
 			throw new Exception("Error Processing Request", 1);
 		}
		$subjects=Schedule::getTeacherSubjects($schoolyear->id,$classID,$teacher->id);
		$subs=[];
		foreach ($subjects as  $value) {
			$subs[$value->id]=$value->name;
		}

		$homeworks=Homework::orderBy('id', 'desc')->where([
			['school_year_id',$schoolyear->id],
 			['class_id',$classID],
 		])->get();
 		$sub_homeworks=[];
 		foreach ($subs as $key => $value) {
 			$sub_homeworks[$key]=$homeworks->where('subject_id',$key);
 		}
 		$viewData=[
	 		'subjects'=>$subs,
	 		'classID'=>$classID,
	 		'homeworks'=>$sub_homeworks

 		];

		return view('teacher/homework',$viewData);
	}

	public function store(Request $request)
	{
		$schoolyear=SchoolYear::active();
		$teacher=Teacher::authTeacher();
		if(!$teacher->subInClass($request->subject_id,$request->class_id,$schoolyear->id)){
			throw new Exception("Error Processing Request", 1);
			
		}
		$date=\Carbon\Carbon::now();
		$homework=new Homework;
		$homework->school_year_id=$schoolyear->id;
 		$homework->subject_id=$request->subject_id;
 		$homework->class_id=$request->class_id;
 		$homework->homework=$request->homework;
 		$homework->teacher_id=$teacher->id;
 		$homework->date=$date;
 		$homework->save();
		return redirect('teacher/myclasses/'.$request->class_id.'/homeworks');
	}
}
