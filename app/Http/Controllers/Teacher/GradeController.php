<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;

use App\Models\Schedule;
use App\Models\SchoolClass;
use App\Models\SchoolYear;
use App\Models\Subject;
use App\Models\Pupil;
use App\Models\Teacher;
use App\Models\Grade;
use App\Models\Attendance;
use Illuminate\Http\Request;
use Exception;

class GradeController extends Controller
{
	

 	public function index($pupil_id)
 	{	
 		$teacher=Teacher::authTeacher();
 		$schoolyear = SchoolYear::active();

 		if(!$teacher->hasPupil($pupil_id,$schoolyear->id)){
 			throw new Exception("Error Processing Request", 1);
 		}
 		$subjects=Subject::all();
 		$subs=[];
 		$classID=Pupil::find($pupil_id)->class_id;

 		foreach ($subjects as $value) {
 			$subs[$value->id]=$value->name;
 		}

 		$grades=Grade::where([
 			['school_year_id',$schoolyear->id],
 			['teacher_id',$teacher->id],
 			['pupil_id',$pupil_id],
 		])->get();

 		$teach_sub=Schedule::getTeacherSubjects($schoolyear->id,$classID,$teacher->id);
 		$gradeSubject=[];
 		foreach ($teach_sub as  $subject) {
 			$gradeSubject[$subject->id]=[];
 		}
 		foreach ($grades as  $grade) {
 			$gradeSubject[$grade->subject_id][]=$grade;
 		}
 		$grade_options=[];
 		for ($i=1; $i <11 ; $i++) { 
 			$grade_options[$i]=$i;
 		}
 		$viewData=[
 			'subjects'=>$gradeSubject,
 			'subs'=>$subs,
 			'grade_options'=>$grade_options,
 			'pupil_id'=>$pupil_id
 		];
 		
 		return view('teacher/pupilgrade',$viewData);
 	}

 	public function store(Request $request)
 	{
 		$grade=new Grade;
 		$schoolyear = SchoolYear::active();
 		$teacher=Teacher::authTeacher();
 		$classId=null;
 		$pupil=Pupil::find($request->pupil_id);
 		if(isset($pupil)){
 			$classId=$pupil->class_id;
 		}
 		if(!$teacher->subInClass($request->subject_id,$classId,$schoolyear->id)){
 			throw new Exception("Error Processing Request", 1);
 		}
 		$grade->school_year_id=$schoolyear->id;
 		$grade->subject_id=$request->subject_id;
 		$grade->grade=$request->grade;
 		$grade->pupil_id=$request->pupil_id;
 		$grade->teacher_id=$teacher->id;
 		$grade->date=$request->date;
 		$grade->save();

 		return redirect('teacher/pupils/'.$request->pupil_id);
 	}

 	


}
