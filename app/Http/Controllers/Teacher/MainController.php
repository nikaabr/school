<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;

use App\Models\Schedule;
use App\Models\SchoolClass;
use App\Models\SchoolYear;
use App\Models\Subject;
use App\Models\Pupil;
use App\Models\Teacher;
use App\Models\Grade;
use App\Models\Attendance;
use Illuminate\Http\Request;
use Exception;

class MainController extends Controller
{
	public function index()
	{	
		$schoolyear = SchoolYear::active();
		$teacher=Teacher::authTeacher();
		return view('teacher/index',['teacher'=>$teacher]);
	}

 	public function classes()
 	{	
 		$teacher=Teacher::authTeacher();
 		$schoolyear = SchoolYear::active();
 		$year=$schoolyear->year();
 		$schedule=Schedule::where([
 			['school_year_id',$schoolyear->id],
 			['teacher_id',$teacher->id],
 		])->get();
 		$uniqueSchedule = $schedule->unique('class_id');
 		$classArr=[];
 		foreach ($uniqueSchedule as  $item) {
 			$classArr[]=$item->SchoolClass;
 		}

 		return view('teacher/myclasses',['classes'=>$classArr,'year'=>$year]);
 	}

 	public function schedule()
 	{
 		$weekday=[];
 		$teacher=Teacher::authTeacher();
 		$schoolyear = SchoolYear::active();
 		$schedule=Schedule::where([
 			['school_year_id',$schoolyear->id],
 			['teacher_id',$teacher->id],
 		])->orderBy('order', 'asc')->get();
 		
 		for ($i = 1; $i <= count(config('school.days')); $i++) {
            $weekday[$i] = $schedule->where('week_day', (string) $i);
        }
 		return view('teacher/schedule',['weekday'=>$weekday]);

 	}

 	public function showPupils($id)
 	{	
 		$teacher=Teacher::authTeacher();
 		$schoolyear = SchoolYear::active();
 		if(!$teacher->hasclass($id,$schoolyear->id)){
 			throw new Exception("Error Processing Request", 1);
 		}
 		$date=\Carbon\Carbon::now()->toFormattedDateString();
 		$pupils=SchoolClass::find($id)->pupils;
 		

 		$teach_sub=Schedule::getTeacherSubjects($schoolyear->id,$id,$teacher->id);
 		$subs=[];
 		foreach ($teach_sub as $value) {
 			$subs[$value->id]=$value->name;
 		}
 		$viewData=[
 		'pupils'=>$pupils,
 		'date'=>$date,
 		'classID'=>$id,
 		'subjects'=>$subs
 		];
 		return view('teacher/showpupils',$viewData);
 	}


}
